﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ennemi : MonoBehaviour
{
    [Header("Animation")]
    [SerializeField] private float DurationScaleUp = 1f;
    [SerializeField] private AnimationCurve AnimCurveScaleUp;

    [Header("Ennemi Action")]
    private bool canRandomHit = false;
    [SerializeField] private float MinTimeBullet = 2f;
    [SerializeField] private float MaxTimeBullet = 6f;
    [SerializeField] private GameObject BulletEnnemi;
    [SerializeField] private GameObject BulletEffectEnnemi;
    private float randomTimeBullet;
    private float delayBullet = 0f;
    private bool startHit = false;
    private bool RayHitPlayer = false;
    private bool isHitPlayer = false;
    [SerializeField] private float durationSeePlayer = 2f;

    [Header("Ennemi Info")]
    public int RewardScore = 50;


    void Start()
    {
        if (InputEffectManager.Instance.IsEffectApparitionInvader)
            StartCoroutine(ApparitionInvader());
    }


    void Update()
    {
        if (!GameManager.Instance.canMove)
            return;
        InvaderAttack();
        if (RayHitPlayer || canRandomHit)
            return;
        checkFirstligne();
    }

    private void checkFirstligne()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down);
        if(hit.collider != null)
        {
            if (hit.transform.tag == "Ennemi")
            {
                canRandomHit = false;
            }
            else if(hit.transform.tag == "Player")
            {
                RayHitPlayer = true;
            }
        }
        else
        {
            canRandomHit = true;
        }
    }

    private void InvaderAttack()
    {
        if (RayHitPlayer)
        {
            if (!isHitPlayer)
            {
                if (!InputEffectManager.Instance.isEffectBullet)
                    Instantiate(BulletEnnemi, transform.position, Quaternion.identity);
                else
                    Instantiate(BulletEffectEnnemi, transform.position, Quaternion.identity);

                isHitPlayer = true;
                delayBullet = 0;
            }
            delayBullet += Time.deltaTime;
            if(delayBullet >= durationSeePlayer)
            {
                delayBullet = 0f;
                RayHitPlayer = false;
            }
        }
        else if (canRandomHit)
        {
            if (!startHit)
            {
                startHit = true;
                randomTimeBullet = Random.Range(MinTimeBullet, MaxTimeBullet);
            }

            delayBullet += Time.deltaTime;
            if(delayBullet >= randomTimeBullet)
            {
                delayBullet = 0f;
                startHit = false;
                if (!InputEffectManager.Instance.isEffectBullet)
                    Instantiate(BulletEnnemi, transform.position, Quaternion.identity);
                else
                    Instantiate(BulletEffectEnnemi, transform.position, Quaternion.identity);
            }
        }
    }

    private IEnumerator ApparitionInvader()
    {
        Vector3 startScale = transform.localScale;
        transform.localScale = Vector3.zero;
        float startTime = Time.time;
        while (Time.time < startTime + DurationScaleUp)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, startScale, AnimCurveScaleUp.Evaluate((Time.time - startTime) / DurationScaleUp));
            yield return null;
        }
        transform.localScale = startScale;
    }
}
