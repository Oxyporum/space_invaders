﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class InputEffectManager : MonoBehaviour
{
    public static InputEffectManager Instance { get; private set; }

    [SerializeField] private GameObject CameraEffect;
    [SerializeField] private GameObject CatodiqueEffect;
    private bool cameraEffectIsActive = true;
    [HideInInspector] public bool IsEffectApparitionInvader = true;
    [HideInInspector] public bool IsEffectDeadInvader = true;
    [HideInInspector] public bool IsEffectScore = true;
    [HideInInspector] public bool IsEffectHitDmg = true;
    [SerializeField] private GameObject EndlessBorderEffect;
    private bool EndlessBorderIsActive = true;
    [SerializeField] private GameObject ParticlScore;
    private bool ParticlScoreIsActive = true;
    [HideInInspector] public bool IsEffectCameraShake = true;
    [HideInInspector] public bool isEffectBullet = true;
    [HideInInspector] public bool isTentacleActive = true;
    [SerializeField] private GameObject FxShipThrust;
    [HideInInspector] public bool isEffectShipThrust;
    [SerializeField] private List<ParticleSystem> LiParticleShipDamage;
    [SerializeField] private GameObject Tentacle;

    void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        InputEffect();
    }

    private void InputEffect()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))  // enable postProcess
        {
            cameraEffectIsActive = !cameraEffectIsActive;
            CameraEffect.GetComponent<PostProcessLayer>().enabled = cameraEffectIsActive;
            CatodiqueEffect.SetActive(cameraEffectIsActive);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) // effect apparition Invader
        {
            IsEffectApparitionInvader = !IsEffectApparitionInvader;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)) // effect death Invader
        {
            IsEffectDeadInvader = !IsEffectDeadInvader;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4)) // effect Score
        {
            IsEffectScore = !IsEffectScore;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5)) // effect hit dammage
        {
            IsEffectHitDmg = !IsEffectHitDmg;
        }
        if (Input.GetKeyDown(KeyCode.Alpha6)) // effect endlessBorder
        {
            EndlessBorderIsActive = !EndlessBorderIsActive;
            EndlessBorderEffect.SetActive(EndlessBorderIsActive);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7)) // effect particle score
        {
            ParticlScoreIsActive = !ParticlScoreIsActive;
            ParticlScore.SetActive(ParticlScoreIsActive);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8)) // effect Camera Shake
        {
            IsEffectCameraShake = !IsEffectCameraShake;
        }
        if (Input.GetKeyDown(KeyCode.Alpha9)) // effect Bullet
        {
            isEffectBullet = !isEffectBullet;
        }
        if (Input.GetKeyDown(KeyCode.Alpha0)) // effect tentacle
        {
            isTentacleActive = !isTentacleActive;
            Tentacle.SetActive(isTentacleActive);
        }
        if (Input.GetKeyDown(KeyCode.F1)) // effect ship
        {
            isEffectShipThrust = !isEffectShipThrust;
            FxShipThrust.SetActive(isEffectShipThrust);
            foreach(ParticleSystem particle in LiParticleShipDamage)
            {
                if(isEffectShipThrust)
                    particle.Play();
                if(!isEffectShipThrust)
                    particle.Stop();
            }
        }
    }
}
