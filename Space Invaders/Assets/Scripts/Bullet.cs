﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float SpeedBullet = 10f;
    public float lifetime;
    public bool isPlayerBullet = false;
    private int up = 1;
    public int RewardScoreBullet = 10;

    void Start()
    {
        if (isPlayerBullet)
            up = 1;
        else
            up = -1;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, up) * SpeedBullet;
        Destroy(gameObject, lifetime);
    }


    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && !isPlayerBullet) // player is hit
        {
            Destroy(gameObject);
            if (InputEffectManager.Instance.isEffectBullet)
            {
                GameObject fxHitPlayer = Instantiate(GameManager.Instance.FxHitPlayer, collision.transform.position, Quaternion.identity) as GameObject;
                Destroy(fxHitPlayer, 3f);
            }
                collision.GetComponent<PlayerMove>().DamageEffect();
        }
        if(collision.tag == "Ennemi" && isPlayerBullet) // Ennemi is hit
        {
            SoundManager.Instance.DestructEnemi();
            // remove object from list
            if (InputEffectManager.Instance.IsEffectDeadInvader)
            {
                GameObject fx = Instantiate(GameManager.Instance.FxHitEnnemi, collision.transform.position, Quaternion.identity);
                Destroy(fx, 1f);
            }
            GameManager.Instance.Score += collision.GetComponent<Ennemi>().RewardScore;
            GameManager.Instance.StartScoreAnim(collision.transform);
            GameManager.Instance.DestroyInvaders(collision.gameObject);
            Destroy(gameObject);
        }
        if(collision.tag == "Bullet" && isPlayerBullet)                    // bullet is hit
        {
            SoundManager.Instance.SoundShipBlock();
            GameManager.Instance.Score += RewardScoreBullet;
            UiManager.Instance.ScoreUpdate();
            if (InputEffectManager.Instance.isEffectBullet)
            {
                GameObject fxBullet = Instantiate(GameManager.Instance.FxHitBullet, transform.position, Quaternion.identity) as GameObject;
                Destroy(fxBullet, 3f);
            }
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}
