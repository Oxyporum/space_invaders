﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; private set; }

    public AudioSource shootSound;
    public AudioSource destructSound;
    public AudioSource ShipDomg;
    public AudioSource ShipDeath;
    public AudioSource ShipBlock;
    public AudioSource ScoreChange;

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayShootSound()
    {
        shootSound.Play();
    }
    public void DestructEnemi()
    {
        destructSound.Play();
    }
    public void ShipDmg()
    {
        ShipDomg.Play();
    }
    public void ShipDead()
    {
        ShipDeath.Play();
    }
    public void SoundShipBlock()
    {
        ShipBlock.Play();
    }
    public void SoundScoreChange()
    {
        ScoreChange.Play();
    }
}
