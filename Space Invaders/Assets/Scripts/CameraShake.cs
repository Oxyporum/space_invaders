﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public static CameraShake Instance { get; private set; }
    Coroutine skakeCoroutine;

    void Awake()
    {
        Instance = this;
    }

    public void StartCameraShake(float duration, float magnitude)
    {
        if (skakeCoroutine != null)
            StopCoroutine(skakeCoroutine);
        if (InputEffectManager.Instance.IsEffectCameraShake)
            skakeCoroutine = StartCoroutine(ShakeCoroutine(duration, magnitude));
    }

    private IEnumerator ShakeCoroutine(float duration, float magnitude)
    {
        Vector3 originalPos = transform.localPosition;

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.localPosition = new Vector3(x, y, 0f) + transform.localPosition;
            elapsed += Time.deltaTime;

            yield return null;
        }
        transform.localPosition = originalPos;
    }
}
