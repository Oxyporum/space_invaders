﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public static UiManager Instance { get; private set; }

    public GameObject gameover;

    [Header("Score")]
    [SerializeField] private Text ScoreText;
    [SerializeField] private Text HighScoreText;
    [SerializeField] private AnimationCurve AnimCurvScore;
    private Vector3 startScale;
    [SerializeField] private float durationAnimScore = 1f;
    [SerializeField] private Vector3 MaxScaleAnim = new Vector3(1.2f, 1.2f, 1.2f);
    private Coroutine ScoreAnimCoroutine;

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        startScale = ScoreText.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && (gameover.activeSelf))

        {
            GameManager.Instance.ReloadScene();
        }
    }

    public void ScoreUpdate()
    {
        SoundManager.Instance.SoundScoreChange();
        ScoreText.text = GameManager.Instance.Score.ToString();
        if (InputEffectManager.Instance.IsEffectScore)
            StopAndStartScoreAnim();
    }

    private void StopAndStartScoreAnim()
    {
        if (ScoreAnimCoroutine != null)
            StopCoroutine(ScoreAnimCoroutine);
        ScoreAnimCoroutine = StartCoroutine(ScoreAnim());
    }

    private IEnumerator ScoreAnim()
    {
            float startTime = Time.time;
            while (Time.time < startTime + durationAnimScore)
            {
                ScoreText.transform.localScale = Vector3.Lerp(startScale, MaxScaleAnim, AnimCurvScore.Evaluate((Time.time - startTime) / durationAnimScore));
                yield return null;
            }
            ScoreText.transform.localScale = startScale;
            yield return null;
    }

    public void SetHighScore()
    {

        if (GameManager.Instance.Score > PlayerPrefs.GetInt("HighScore"))
            PlayerPrefs.SetInt("HighScore", GameManager.Instance.Score);
        HighScoreText.text = "High Score : " + PlayerPrefs.GetInt("HighScore");
    }

}
