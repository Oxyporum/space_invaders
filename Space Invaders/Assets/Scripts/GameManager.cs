﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [HideInInspector] public int Score = 0;

    [Header("MargeScreen")]
    [Range(0f, 1f)] [SerializeField] private float MargingSize = 0.91f;
    [HideInInspector] public float MargingPos;  // margingPos positif = RightMarge else leftMarge

    [Header("Ennemi Instantiation")]
    public List<LigneEnnemi> LiLigneEnnemi = new List<LigneEnnemi>();
    public List<GameObject> LiPrefabFormation;
    public List<GameObject> LiEnnemi;
    public int LigneCountMin = 2;
    public int LigneCountMax = 5;
    public float SpawnSpeed = 0.1f;
    public float startPosVertical = 4.5f;

    [Header("EnnemiMove")]
    public float SpeedEnnemi = 5f;
    private float startSpeedEnnemi = 0f;
    private float SensEnemiMove = 1f;
    private bool isStartMovingVertical = false;
    private bool isMovingHorizontal = true;
    private bool isStartingMovingHorizontal = true;
    private float actualPosVertical;
    [HideInInspector] public bool canMove = false;

    [Header("Anim Invader Death")]
    [SerializeField] private AnimationCurve AnimCurveDeath;
    [SerializeField] private float SpeedAnimDeath = 1f;
    [SerializeField] GameObject KoScore;
    [SerializeField] private Transform PosTextScore;
    public List<ParticleSystem> EffectToScore;

    [Header("FX")]
    public GameObject FxHitBullet;
    public GameObject FxHitPlayer;
    public GameObject FxHitEnnemi;

    void Awake()
    {
        Instance = this;
    }
    
    void Start()
    {
        GetScreenSize();
        startSpeedEnnemi = SpeedEnnemi;
        StartCoroutine(InstantiateInvaders());

    }
    
    void Update()
    {
        MoveEnnemi();
    }

    private IEnumerator InstantiateInvaders()
    {
        canMove = false;
        float verticalPos = startPosVertical;
        PlacementFormation formation = LiPrefabFormation[0].GetComponent<PlacementFormation>();

        int randomNbLigne = Random.Range(LigneCountMin, LigneCountMax);
        for (int i = 0; i < randomNbLigne; i++)
        {
            Vector3 VerticalSpace = new Vector3(0f, verticalPos, 0f);
            LiLigneEnnemi.Add(new LigneEnnemi { });
            int randomEnnemi = Random.Range(0, LiEnnemi.Count - 1);
            for (int j = 0; j < formation.LiPosition.Count - 1; j++)
            {
                GameObject ennemi = Instantiate(LiEnnemi[randomEnnemi]) as GameObject;
                LiLigneEnnemi[i].LiPrefabEnnemi.Add(ennemi);
                ennemi.transform.position = formation.LiPosition[j].position + VerticalSpace;
                if (InputEffectManager.Instance.IsEffectApparitionInvader)
                    yield return new WaitForSeconds(SpawnSpeed);
                else
                    yield return null;
                if (SpawnSpeed > 0.05f)
                    SpawnSpeed -= 0.01f;
            }
            verticalPos += 1f;
        }
        actualPosVertical = startPosVertical;
        SpeedEnnemi = startSpeedEnnemi;
        canMove = true;
    }

    private void MoveEnnemi()
    {
        if (!canMove)
            return;
        foreach(LigneEnnemi ligne in LiLigneEnnemi)
        {
            if (isMovingHorizontal)
            {
                if (isStartingMovingHorizontal)
                    isStartingMovingHorizontal = false;
                foreach (GameObject ennemi in ligne.LiPrefabEnnemi)
                {
                    ennemi.transform.position = ennemi.transform.position + new Vector3(SpeedEnnemi * Time.deltaTime * SensEnemiMove, 0f, 0f);
                }
            }
            else
            {
                if (isStartMovingVertical)
                {
                    isStartMovingVertical = false;
                    actualPosVertical -= 0.5f;
                    //Debug.Log(actualPosVertical);
                }
                foreach (GameObject ennemi in ligne.LiPrefabEnnemi)
                {
                    ennemi.transform.position = ennemi.transform.position + new Vector3(0f, SpeedEnnemi * Time.deltaTime * -1, 0f);
                    if (ennemi.transform.position.y <= actualPosVertical)
                    {
                        //Debug.Log(ennemi.transform.position.y + "  " + actualPosVertical);
                        isMovingHorizontal = true;
                        isStartingMovingHorizontal = true;
                    }
                }
            }
            Vector3 ennemiLeft = ligne.LiPrefabEnnemi[0].transform.position;
            Vector3 ennemiRight = ligne.LiPrefabEnnemi[ligne.LiPrefabEnnemi.Count - 1].transform.position;
            if (ennemiLeft.x <= -MargingPos && isMovingHorizontal && !isStartingMovingHorizontal)
            {
                SensEnemiMove = 1f;
                isStartMovingVertical = true;
                isMovingHorizontal = false;
            }
            if (ennemiRight.x >= MargingPos && isMovingHorizontal && !isStartingMovingHorizontal)
            {
                SensEnemiMove = -1f;
                isStartMovingVertical = true;
                isMovingHorizontal = false;
            }
        }
    }

    void GetScreenSize()
    {
        Vector3 rightMarge = Camera.main.ViewportToWorldPoint(new Vector3(MargingSize, 0.5f, 1f));
        MargingPos = rightMarge.x;
    }

    public void DestroyInvaders(GameObject Invader)
    {
        for (int i = 0; i < LiLigneEnnemi.Count; i++)
        {
            GameObject ennemi = LiLigneEnnemi[i].LiPrefabEnnemi.Find(r => r == Invader);
            LiLigneEnnemi[i].LiPrefabEnnemi.Remove(ennemi);
            Destroy(Invader);

            if (LiLigneEnnemi[i].LiPrefabEnnemi.Count == 0)
            {
                LiLigneEnnemi.Remove(LiLigneEnnemi[i]);
                SpeedEnnemi += 1f;
            }
        }
        Debug.Log(LiLigneEnnemi.Count);
        if(LiLigneEnnemi.Count == 0)
        {
            StartCoroutine(InstantiateInvaders());
            Debug.Log("instantiate invaders");
        }
    }

    public void ClearAllInvaders()
    {
        for (int i = 0; i < LiLigneEnnemi.Count; i++)
        {
            for (int j = 0; j < LiLigneEnnemi[i].LiPrefabEnnemi.Count; j++)
            {
                Destroy(LiLigneEnnemi[i].LiPrefabEnnemi[j]);
            }
        }
        LiLigneEnnemi.Clear();
    }

    public void StartScoreAnim(Transform invaders)
    {
        StartCoroutine(AnimInvaderDeath(invaders));
    }

    private IEnumerator AnimInvaderDeath(Transform invaders)
    {

        if (InputEffectManager.Instance.IsEffectDeadInvader)
        {
            GameObject animDeath = Instantiate(KoScore, invaders.position, Quaternion.identity) as GameObject;
            Vector3 startPosition = animDeath.transform.position;
            float distance = Vector2.Distance(startPosition, PosTextScore.position);
            float duration = distance / SpeedAnimDeath;
            float startTime = Time.time;
            while (Time.time < startTime + duration)
            {
                animDeath.transform.position = Vector3.Lerp(startPosition, PosTextScore.position, AnimCurveDeath.Evaluate((Time.time - startTime) / duration));
                yield return null;
            }
            KoScore.transform.position = PosTextScore.position;
            Destroy(animDeath);
        }
        foreach(ParticleSystem particle in EffectToScore)
        {
            particle.Play();
        }
        UiManager.Instance.ScoreUpdate();
        yield return null;
    }


    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}

[System.Serializable]
public class LigneEnnemi
{
    public List<GameObject> LiPrefabEnnemi = new List<GameObject>();
}
