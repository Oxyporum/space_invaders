﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float Speed = 5f;
    public GameObject bullet;
    public GameObject bulletEffect;

    float moveHorizontal;
    float canMove = 0; // Zero = no move , 1 = move
    private bool canHit = true;
    public float CouldownHit = 1f;
    private float delay = 0f;
    public List<GameObject> LiDamageEffect;
    private int life = 3;
    public GameObject GameOver;
    [SerializeField] private GameObject HitDmgPlayer;
    public List<ParticleSystem> LiFxShootFlash;

    private void Start()
    {

    }

    void Update()
    {
        if (!GameManager.Instance.canMove)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            return;
        }
        moveHorizontal = Input.GetAxisRaw("Horizontal");

        if (Mathf.Abs(transform.position.x) < Mathf.Abs(GameManager.Instance.MargingPos) ||  // si le joueur se déplace entre les marge
           (transform.position.x < -GameManager.Instance.MargingPos && moveHorizontal > 0.5f) || // si le joueur est collé à gauche et veut aller à droite
           (transform.position.x > GameManager.Instance.MargingPos && moveHorizontal < 0.5f)) // si le joueur est collé à droite et veut aller à gauche
        {
            canMove = 1f;
        }

        else
        {
            canMove = 0f;
        }

        GetComponent<Rigidbody2D>().velocity = new Vector2(moveHorizontal, 0) * Speed * canMove;

        if (Input.GetKeyDown(KeyCode.Space) && canHit)
        {
            if (!InputEffectManager.Instance.isEffectBullet)
                Instantiate(bullet, transform.position, Quaternion.identity);
            else
            {
                Instantiate(bulletEffect, transform.position, Quaternion.identity);
                foreach(ParticleSystem fx in LiFxShootFlash)
                {
                    fx.Play();
                }
            }
            SoundManager.Instance.PlayShootSound();
            canHit = false;
        }
        if (!canHit)
        {
            delay += Time.deltaTime;
            if (delay >= CouldownHit)
            {
                delay = 0f;
                canHit = true;
            }
        }
    }

    public void DamageEffect()
    {

        if(InputEffectManager.Instance.IsEffectHitDmg)
            HitDmgPlayer.GetComponent<ParticleSystem>().Play();
        if (life == 3)
        {
            SoundManager.Instance.ShipDmg();
            life--;
            LiDamageEffect[0].SetActive(true);
            CameraShake.Instance.StartCameraShake(0.2f, 0.1f);
            return;
        }
        if (life == 2)
        {
            SoundManager.Instance.ShipDmg();
            life--;
            LiDamageEffect[1].SetActive(true);
            CameraShake.Instance.StartCameraShake(0.2f, 0.1f);
            return;
        }
        if (life == 1) // player is dead
        {
            SoundManager.Instance.ShipDead();
            life--;
            GameManager.Instance.ClearAllInvaders();
            UiManager.Instance.SetHighScore();
            GameManager.Instance.canMove = false;
            GameOver.transform.gameObject.SetActive(true);
            CameraShake.Instance.StartCameraShake(0.3f, 0.1f);

            Destroy(gameObject);

            return;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameManager.Instance.ClearAllInvaders();
        UiManager.Instance.SetHighScore();
        GameManager.Instance.canMove = false;
        GameOver.transform.gameObject.SetActive(true);
        CameraShake.Instance.StartCameraShake(0.3f, 0.1f);
        Destroy(gameObject);
    }
}